﻿using AutoMapper;
using BoxinatorAPI.Controllers;
using BoxinatorAPI.Models;
using BoxinatorAPI.Models.Domains;
using BoxinatorAPI.Models.DTOs.User;
using BoxinatorAPI.Profiles;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BoxinatorAPITest.Controllers
{
    class AccountControllerTests
    {
        private DbContextOptions<BoxinatorDbContext> dbContextOptions = new DbContextOptionsBuilder<BoxinatorDbContext>()
        .UseInMemoryDatabase(databaseName: "TestAccountDb")
        .Options;
        private BoxinatorDbContext context;
        private AccountController controller;
        private int currentId = 4;
        private Mapper mapper;

        [OneTimeSetUp]
        public void Setup()
        {
            context = new BoxinatorDbContext(dbContextOptions);
            SeedDb();

            mapper = new Mapper(new MapperConfiguration(conf => conf.AddProfiles(new Profile[] { new UserProfile(), new ShipmentProfile(), new CountryProfile() })));
            controller = new AccountController(context, mapper);
            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, "auth0|612f2c14eeeb2e0068b262b0")
            }, "mock"));
            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = user }
            };
        }

        private void SeedDb()
        {
            var countries = new List<Country>
            {
                new Country { Id = 1, Name = "Sweden", CountryMultiplier = 0 },
                new Country { Id = 2, Name = "Norway", CountryMultiplier = 0 },
                new Country { Id = 3, Name = "Denmark", CountryMultiplier = 0 },
                new Country { Id = 4, Name = "Argentina", CountryMultiplier = 5 },
                new Country { Id = 5, Name = "Germany", CountryMultiplier = 3 }
            };
            context.Countries.AddRange(countries);

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    FirstName = "Patric",
                    LastName = "Bju",
                    Email = "patric.bjurman@se.experis.com",
                    Sub = "auth0|6136084e2c6a4500693fefa9",
                    BirthDate = new DateTime(1990, 1, 23),
                    Contactnumber = "000001",
                    CountryId = 2,
                    ZipCode = 11122
                },
                new User
                {
                    Id = 2,
                    FirstName = "Emil",
                    LastName = "Cle",
                    Email = "emilcle@gmail.com",
                    Sub = "auth0|612f2c14eeeb2e0068b262b0",
                    BirthDate = new DateTime(1990, 1, 23),
                    Contactnumber = "000000",
                    CountryId = 1,
                    ZipCode = 11122
                },
                new User
                {
                    Id = 3,
                    FirstName = null,
                    LastName = null,
                    Email = "emil.clemedson@se.experis.com",
                    Sub = null,
                    BirthDate = null,
                    Contactnumber = null,
                    CountryId = null,
                    ZipCode = null
                },
                new User
                {
                    Id = 4,
                    FirstName = "test",
                    LastName = "test",
                    Email = "test@gmail.com",
                    Sub = "auth0|612f2c14eeeb2e0b262b0",
                    BirthDate = new DateTime(1990, 1, 23),
                    Contactnumber = "000000",
                    CountryId = 1,
                    ZipCode = 11122
                },
            };

            context.Users.AddRange(users);
            context.SaveChanges();
        }


        [Test]
        public async Task Get_ExistingUserGetByToken_GetUser()
        {

            var res = await controller.Get();
            Xunit.Assert.IsType<OkObjectResult>(res.Result);
            var ok = (OkObjectResult)res.Result;
            var user = (UserRead)ok.Value;
            user.FirstName.Should().Be("Emil");
            user.LastName.Should().Be("Cle");
            user.Email.Should().Be("emilcle@gmail.com");
            user.BirthDate.Should().Be(new DateTime(1990, 1, 23));
            user.Contactnumber.Should().Be("000000");
            user.Country.Id.Should().Be(1);
            user.Country.Name.Should().Be("Sweden");

        }


        [Test]
        public async Task Get_ExistingUserGetById_GetUser()
        {
            var res = await controller.Get(2);
            Xunit.Assert.IsType<OkObjectResult>(res.Result);
            var ok = (OkObjectResult)res.Result;
            var user = (UserRead)ok.Value;
            user.FirstName.Should().Be("Emil");
            user.LastName.Should().Be("Cle");
            user.Email.Should().Be("emilcle@gmail.com");
            user.BirthDate.Should().Be(new DateTime(1990, 1, 23));
            user.Contactnumber.Should().Be("000000");
            user.Country.Id.Should().Be(1);
            user.Country.Name.Should().Be("Sweden");
        }

        [Test]
        public async Task Get_NotExistingUserGetById_NotFoundResult()
        {
            var res = await controller.Get(-1);
            Xunit.Assert.IsType<NotFoundResult>(res.Result);
        }


        [Test]
        public async Task Post_PostingNewUser_Sucsses()
        {
            var newUser = new UserCreate
            {
                FirstName = "test first",
                LastName = "test last",
                Email = "test.test@se.experis.com",
                Sub = "auth0|test",
                BirthDate = new DateTime(1990, 1, 23),
                Contactnumber = "000001",
                CountryId = 2,
                ZipCode = 11122
            };
            var res = await controller.Post(newUser);
            currentId++;
            int id = currentId;
            Xunit.Assert.IsType<CreatedAtActionResult>(res.Result);

            var create = (CreatedAtActionResult)res.Result;
            var user = (UserRead)create.Value;

            user.Id.Should().Be(id);
            user.FirstName.Should().Be("test first");
            user.LastName.Should().Be("test last");
            user.Email.Should().Be("test.test@se.experis.com");
            user.BirthDate.Should().Be(new DateTime(1990, 1, 23));
            user.Contactnumber.Should().Be("000001");

            user.Country.Id.Should().Be(2);
            user.Country.Name.Should().Be("Norway");
        }

        [Test]
        public async Task Post_PostingGuestUser_Sucsses()
        {
            var newUser = new UserCreate
            {
                FirstName = "test first",
                LastName = "test last",
                Email = "emil.clemedson@se.experis.com",
                Sub = "auth0|test",
                BirthDate = new DateTime(1990, 1, 23),
                Contactnumber = "000001",
                CountryId = 2,
                ZipCode = 11122
            };
            var res = await controller.Post(newUser);
            Xunit.Assert.IsType<CreatedAtActionResult>(res.Result);

            var create = (CreatedAtActionResult)res.Result;
            var user = (UserRead)create.Value;
            user.Id.Should().Be(3);
            user.FirstName.Should().Be("test first");
            user.LastName.Should().Be("test last");
            user.Email.Should().Be("emil.clemedson@se.experis.com");
            user.BirthDate.Should().Be(new DateTime(1990, 1, 23));
            user.Contactnumber.Should().Be("000001");

            user.Country.Id.Should().Be(2);
            user.Country.Name.Should().Be("Norway");
        }


        [Test]
        public async Task Put_UpdatingUser_Sucsses()
        {
            int id = 4;
            var newUserData = new UserEdit
            {
                FirstName = "test first",
                LastName = "test last",
                BirthDate = new DateTime(1990, 5, 23),
                Contactnumber = "010201",
                CountryId = 4,
                ZipCode = 13322
            };

            var res = await controller.Put(id, newUserData);
            Xunit.Assert.IsType<NoContentResult>(res);

            var user = await context.Users.FindAsync(id);

            user.FirstName.Should().Be(newUserData.FirstName);
            user.LastName.Should().Be(newUserData.LastName);
            user.BirthDate.Should().Be(newUserData.BirthDate);
            user.Contactnumber.Should().Be(newUserData.Contactnumber);
            user.CountryId.Should().Be(newUserData.CountryId);
            user.ZipCode.Should().Be(newUserData.ZipCode);
        }



        [Test]
        public async Task Put_UpdatingNotExistingUser_Sucsses()
        {
            int id = 0;
            var newUserData = new UserEdit
            {
                FirstName = "test first",
                LastName = "test last",
                BirthDate = new DateTime(1990, 5, 23),
                Contactnumber = "010201",
                CountryId = 4,
                ZipCode = 13322
            };

            var res = await controller.Put(id, newUserData);
            Xunit.Assert.IsType<NotFoundResult>(res);
        }


        [Test]
        public async Task Delete_DeleteNotExistingUser_NotFound()
        {

            int id = 0;
            var res = await controller.Delete(id);
            Xunit.Assert.IsType<NotFoundResult>(res);
        }

        [Test]
        public async Task Delete_DeleteExistingUser_Sucsses()
        {

            int id = 1;
            var res = await controller.Delete(id);
            Xunit.Assert.IsType<NoContentResult>(res);
            (await context.Users.SingleOrDefaultAsync(u => u.Id == id)).Should().BeNull();
        }

    }
}