using AutoMapper;
using BoxinatorAPI.Models;
using BoxinatorAPI.Models.Domains;
using BoxinatorAPI.Models.DTOs.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BoxinatorAPI.Controllers
{
    [Route("api/v1/account")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class AccountController : ControllerBase
    {
        private readonly BoxinatorDbContext _context;
        private readonly IMapper _mapper;

        public AccountController(BoxinatorDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Returns the current user.
        /// </summary>
        /// <response code = "200">Request ok. User found.</response>
        /// <response code = "404">Not found. User not found.</response>
        /// <returns>Returns the user.</returns>
        [HttpGet()]
        [Authorize]
        public async Task<ActionResult<UserRead>> Get()
        {
            string sub = User.Identity.Name;
            Console.WriteLine(sub);
            var user = await _context.Users.Include(u => u.Country).SingleOrDefaultAsync(u => u.Sub == sub);
            if (user == null)
            {
                return NotFound();
            }
            return Ok(_mapper.Map<UserRead>(user));
        }



        /// <summary>
        /// Returns a user with the corresponding id
        /// </summary>
        /// <param name="id">Id for the user.</param>
        /// <response code = "200">Request ok. User found.</response>
        /// <response code = "404">Not found. User not found.</response>
        /// <returns>Returns the user.</returns>
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<UserRead>> Get(int id)
        {
            var user = await _context.Users.Include(u => u.Country).SingleOrDefaultAsync(u => u.Id == id);
            if (user == null)
            {
                return NotFound();
            }
            return Ok(_mapper.Map<UserRead>(user));
        }

        /// <summary>
        /// Post a new user to the database.
        /// </summary>
        /// <param name="user">The user to post.</param>
        /// <response code = "201">User created and added to the database.</response>
        /// <response code = "400">Bad request. Something wrong with the request. Probably in the Body.</response>
        /// <returns>Returns the user.</returns>
        [HttpPost]
        public async Task<ActionResult<UserRead>> Post([FromBody] UserCreate user)
        {
            User domainUser = _mapper.Map<User>(user);
            var guestUser = await _context.Users.SingleOrDefaultAsync(u => u.Email == user.Email);

            if (guestUser != null)
            {
                if (guestUser.Sub != null)
                {
                    return BadRequest();
                }

                guestUser.FirstName = domainUser.FirstName;
                guestUser.LastName = domainUser.LastName;
                guestUser.BirthDate = domainUser.BirthDate;
                guestUser.CountryId = domainUser.CountryId;
                guestUser.ZipCode = domainUser.ZipCode;
                guestUser.Contactnumber = domainUser.Contactnumber;
                guestUser.Sub = domainUser.Sub;

                try
                {
                    _context.Entry(guestUser).State = EntityState.Modified;
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
                UserRead guestUserRead = _mapper.Map<UserRead>(guestUser);
                return CreatedAtAction("Get", new { Id = guestUserRead.Id }, guestUserRead);
            } 

            try
            {
                _context.Users.Add(domainUser);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            UserRead newUser = _mapper.Map<UserRead>(domainUser);
            return CreatedAtAction("Get", new { Id = newUser.Id}, newUser);
        }

        /// <summary>
        /// Updates a user.
        /// </summary>
        /// <param name="id">Id of the user to update.</param>
        /// <param name="updatedUser">The data to update the user with.</param>
        /// <response code = "204">No content to send for this request.</response>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, UserEdit updatedUser)
        {
            User presentUser = await _context.Users.SingleOrDefaultAsync(u => u.Id == id);
            if (presentUser == null)
            {
                return NotFound();
            }

            presentUser.FirstName = updatedUser.FirstName;
            presentUser.LastName = updatedUser.LastName;
            presentUser.BirthDate = updatedUser.BirthDate;
            presentUser.CountryId = updatedUser.CountryId;
            presentUser.ZipCode = updatedUser.ZipCode;
            presentUser.Contactnumber = updatedUser.Contactnumber;

            try
            {
                _context.Entry(presentUser).State = EntityState.Modified;
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }
            return NoContent();
        }

        /// <summary>
        /// Deletes a user from the database.
        /// </summary>
        /// <param name="id">Id of the user to delete.</param>
        /// <response code = "204">No content to send for this request.</response>
        /// <response code = "404">User not found.</response>
        /// <response cod = "400">Bad request. Something wrong with the request. Probably in the Body.</response>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [Authorize("Admin")]
        public async Task<ActionResult> Delete(int id)
        {
            
            var user = await _context.Users.FindAsync(id);

            if (user == null)
            {
                return NotFound();
            }

            try
            {
                _context.Users.Remove(user);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                return BadRequest();
            }

            return NoContent();
        }
    }

}

