﻿using AutoMapper;
using BoxinatorAPI.Models;
using BoxinatorAPI.Models.Domains;
using BoxinatorAPI.Models.DTOs.Settings;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BoxinatorAPI.Controllers
{
    [Route("api/v1/settings")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class SettingsController : ControllerBase
    {

        private readonly BoxinatorDbContext _dbContext;
        private readonly IMapper _mapper;

        public SettingsController(BoxinatorDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all countries.
        /// </summary>
        /// <response code = "200">Request ok.</response>
        /// <returns>A list with all the countries.</returns>
        [HttpGet("countries")]
        public async Task<ActionResult<IEnumerable<CountryRead>>> Get()
        {
            var countries = await _dbContext.Countries.OrderBy(c => c.Name).ToArrayAsync();
            var countriesDTO = _mapper.Map<List<CountryRead>>(countries);
            return Ok(countriesDTO);
        }

        /// <summary>
        /// Add a country.
        /// </summary>
        /// <param name="character">The country you want to add.</param>
        /// <response code = "201">Created. Country created.</response>
        /// <response code = "400">Bad request. Something wrong with the request. Probably in the Body.</response>
        /// <returns>Returns the country.</returns>
        [HttpPost("countries")]
        [Authorize("Admin")]
        public async Task<ActionResult<CountryRead>> Post([FromBody]CountryCreate country)
        {
            var domainCountry = _mapper.Map<Country>(country);

            try
            {
                await _dbContext.AddAsync(domainCountry);
                await _dbContext.SaveChangesAsync();
            } catch(DbUpdateException)
            {
                return BadRequest();
            }

            return CreatedAtAction("Get", new { id = domainCountry.Id }, _mapper.Map<CountryRead>(domainCountry));
        }

        /// <summary>
        /// Update all properties of a country.
        /// </summary>
        /// <param name="id">The id of the country to be updated.</param>
        /// <param name="character">The new state of the country.</param>
        /// <response code = "400">Bad request. Something wrong with the request. Probably in the Body.</response>
        /// <response code = "404">Not found. Country not found.</response>
        /// <response code = "200">Request ok. The country was updated.</response>
        /// <returns></returns>
        [HttpPut("countries/{id}")]
        [Authorize("Admin")]
        public async Task<ActionResult> Put(int id, [FromBody] CountryEdit country)
        {
            var updatedCountry = await _dbContext.Countries.SingleOrDefaultAsync((c) => c.Id == id);
            if (updatedCountry == null)
            {
                return NotFound();
            }

            updatedCountry.Name = country.Name;
            updatedCountry.CountryMultiplier = country.CountryMultiplier;


            try
            {
                _dbContext.Entry(updatedCountry).State = EntityState.Modified;
                await _dbContext.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return NoContent();
        }

    }
}
