﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BoxinatorAPI.Migrations
{
    public partial class addedSeededData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 4,
                column: "CountryMultiplier",
                value: 5);

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 5,
                column: "CountryMultiplier",
                value: 3);

            migrationBuilder.UpdateData(
                table: "ShipmentStatuses",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "ShipmentId", "Status" },
                values: new object[] { new DateTime(2021, 2, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 3 });

            migrationBuilder.UpdateData(
                table: "ShipmentStatuses",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "ShipmentId", "Status" },
                values: new object[] { new DateTime(2021, 2, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), 2, 4 });

            migrationBuilder.UpdateData(
                table: "ShipmentStatuses",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "ShipmentId" },
                values: new object[] { new DateTime(2021, 2, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), 3 });

            migrationBuilder.UpdateData(
                table: "ShipmentStatuses",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "ShipmentId", "Status" },
                values: new object[] { new DateTime(2021, 2, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), 4, 2 });

            migrationBuilder.UpdateData(
                table: "Shipments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DestinationCountryId", "UserId" },
                values: new object[] { 1, 1 });

            migrationBuilder.UpdateData(
                table: "Shipments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DestinationCountryId", "UserId" },
                values: new object[] { 2, 2 });

            migrationBuilder.UpdateData(
                table: "Shipments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "DestinationCountryId", "UserId" },
                values: new object[] { 3, 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                column: "LastName",
                value: "Bju");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Email", "FirstName", "LastName", "Sub" },
                values: new object[] { "emilcle@gmail.com", "Emil", "Cle", "auth0|612f2c14eeeb2e0068b262b0" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Email", "FirstName", "LastName", "Sub" },
                values: new object[] { "emil.clemedson@gmail.com", "Emil", "Clemedson", "auth0|6135fffc1ee4b60069411721" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "AccountType", "BirthDate", "Contactnumber", "CountryId", "Email", "FirstName", "LastName", "Sub", "ZipCode" },
                values: new object[] { 4, null, null, null, null, "christian.andersz@se.experis.com", "Christian", "Andersz", "auth0|6130b87feeeb2e0068b313ad", null });

            migrationBuilder.UpdateData(
                table: "Shipments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "DestinationCountryId", "UserId" },
                values: new object[] { 4, 4 });

            migrationBuilder.InsertData(
                table: "Shipments",
                columns: new[] { "Id", "BoxColour", "DestinationCountryId", "ReceiverName", "TotalCost", "UserId", "WeightOption" },
                values: new object[] { 5, null, 5, null, 0, 4, 0 });

            migrationBuilder.UpdateData(
                table: "ShipmentStatuses",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "ShipmentId" },
                values: new object[] { new DateTime(2021, 2, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), 5 });

            migrationBuilder.InsertData(
                table: "ShipmentStatuses",
                columns: new[] { "Id", "CreatedAt", "ShipmentId", "Status" },
                values: new object[,]
                {
                    { 6, new DateTime(2021, 3, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), 5, 2 },
                    { 7, new DateTime(2021, 4, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), 5, 1 },
                    { 8, new DateTime(2021, 5, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), 5, 3 },
                    { 9, new DateTime(2021, 6, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), 5, 4 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "ShipmentStatuses",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "ShipmentStatuses",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "ShipmentStatuses",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "ShipmentStatuses",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Shipments",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 4,
                column: "CountryMultiplier",
                value: 0);

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 5,
                column: "CountryMultiplier",
                value: 0);

            migrationBuilder.UpdateData(
                table: "ShipmentStatuses",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "ShipmentId", "Status" },
                values: new object[] { new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 0 });

            migrationBuilder.UpdateData(
                table: "ShipmentStatuses",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "ShipmentId", "Status" },
                values: new object[] { new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 0 });

            migrationBuilder.UpdateData(
                table: "ShipmentStatuses",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "ShipmentId" },
                values: new object[] { new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null });

            migrationBuilder.UpdateData(
                table: "ShipmentStatuses",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "ShipmentId", "Status" },
                values: new object[] { new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 0 });

            migrationBuilder.UpdateData(
                table: "ShipmentStatuses",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "ShipmentId" },
                values: new object[] { new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null });

            migrationBuilder.UpdateData(
                table: "Shipments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "DestinationCountryId", "UserId" },
                values: new object[] { null, null });

            migrationBuilder.UpdateData(
                table: "Shipments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "DestinationCountryId", "UserId" },
                values: new object[] { null, null });

            migrationBuilder.UpdateData(
                table: "Shipments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "DestinationCountryId", "UserId" },
                values: new object[] { null, null });

            migrationBuilder.UpdateData(
                table: "Shipments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "DestinationCountryId", "UserId" },
                values: new object[] { null, null });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                column: "LastName",
                value: "Bjurman");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Email", "FirstName", "LastName", "Sub" },
                values: new object[] { "patric.bjurman@se.experis.com", "Patric", "Bjurman", "auth0|6136084e2c6a4500693fefa9" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Email", "FirstName", "LastName", "Sub" },
                values: new object[] { "patric.bjurman@se.experis.com", "Patric", "Bjurman", "auth0|6136084e2c6a4500693fefa9" });
        }
    }
}
