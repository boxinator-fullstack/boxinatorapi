﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BoxinatorAPI.Migrations
{
    public partial class addSubToUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Sub",
                table: "Users",
                type: "nvarchar(42)",
                maxLength: 42,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Shipments",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Shipments_UserId",
                table: "Shipments",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Shipments_Users_UserId",
                table: "Shipments",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Shipments_Users_UserId",
                table: "Shipments");

            migrationBuilder.DropIndex(
                name: "IX_Shipments_UserId",
                table: "Shipments");

            migrationBuilder.DropColumn(
                name: "Sub",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Shipments");
        }
    }
}
