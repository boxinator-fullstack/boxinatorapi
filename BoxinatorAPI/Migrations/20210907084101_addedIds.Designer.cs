﻿// <auto-generated />
using System;
using BoxinatorAPI.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace BoxinatorAPI.Migrations
{
    [DbContext(typeof(BoxinatorDbContext))]
    [Migration("20210907084101_addedIds")]
    partial class addedIds
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.9")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("BoxinatorAPI.Models.Domains.Country", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("CountryMultiplier")
                        .HasColumnType("int");

                    b.Property<string>("Name")
                        .HasMaxLength(100)
                        .HasColumnType("nvarchar(100)");

                    b.HasKey("Id");

                    b.ToTable("Countries");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            CountryMultiplier = 0,
                            Name = "Sweden"
                        },
                        new
                        {
                            Id = 2,
                            CountryMultiplier = 0,
                            Name = "Norway"
                        },
                        new
                        {
                            Id = 3,
                            CountryMultiplier = 0,
                            Name = "Denmark"
                        },
                        new
                        {
                            Id = 4,
                            CountryMultiplier = 0,
                            Name = "Argentina"
                        },
                        new
                        {
                            Id = 5,
                            CountryMultiplier = 0,
                            Name = "Germany"
                        });
                });

            modelBuilder.Entity("BoxinatorAPI.Models.Domains.Shipment", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("BoxColour")
                        .HasMaxLength(20)
                        .HasColumnType("nvarchar(20)");

                    b.Property<int?>("DestinationCountryId")
                        .HasColumnType("int");

                    b.Property<string>("ReceiverName")
                        .HasMaxLength(100)
                        .HasColumnType("nvarchar(100)");

                    b.Property<int>("TotalCost")
                        .HasColumnType("int");

                    b.Property<int?>("UserId")
                        .HasColumnType("int");

                    b.Property<int>("WeightOption")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("DestinationCountryId");

                    b.HasIndex("UserId");

                    b.ToTable("Shipments");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            TotalCost = 0,
                            WeightOption = 0
                        },
                        new
                        {
                            Id = 2,
                            TotalCost = 0,
                            WeightOption = 0
                        },
                        new
                        {
                            Id = 3,
                            TotalCost = 0,
                            WeightOption = 0
                        },
                        new
                        {
                            Id = 4,
                            TotalCost = 0,
                            WeightOption = 0
                        });
                });

            modelBuilder.Entity("BoxinatorAPI.Models.Domains.ShipmentStatus", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("datetime2");

                    b.Property<int?>("ShipmentId")
                        .HasColumnType("int");

                    b.Property<int>("Status")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("ShipmentId");

                    b.ToTable("ShipmentStatuses");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Status = 0
                        },
                        new
                        {
                            Id = 2,
                            CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Status = 0
                        },
                        new
                        {
                            Id = 3,
                            CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Status = 0
                        },
                        new
                        {
                            Id = 4,
                            CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Status = 0
                        },
                        new
                        {
                            Id = 5,
                            CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Status = 0
                        });
                });

            modelBuilder.Entity("BoxinatorAPI.Models.Domains.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("AccountType")
                        .HasColumnType("int");

                    b.Property<DateTime?>("BirthDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("Contactnumber")
                        .HasMaxLength(14)
                        .HasColumnType("nvarchar(14)");

                    b.Property<int?>("CountryId")
                        .HasColumnType("int");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasMaxLength(100)
                        .HasColumnType("nvarchar(100)");

                    b.Property<string>("FirstName")
                        .HasMaxLength(100)
                        .HasColumnType("nvarchar(100)");

                    b.Property<string>("LastName")
                        .HasMaxLength(100)
                        .HasColumnType("nvarchar(100)");

                    b.Property<string>("Sub")
                        .HasMaxLength(42)
                        .HasColumnType("nvarchar(42)");

                    b.Property<int?>("ZipCode")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("CountryId");

                    b.ToTable("Users");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Email = "patric.bjurman@se.experis.com",
                            FirstName = "Patric",
                            LastName = "Bjurman",
                            Sub = "auth0|6136084e2c6a4500693fefa9"
                        },
                        new
                        {
                            Id = 2,
                            Email = "patric.bjurman@se.experis.com",
                            FirstName = "Patric",
                            LastName = "Bjurman",
                            Sub = "auth0|6136084e2c6a4500693fefa9"
                        },
                        new
                        {
                            Id = 3,
                            Email = "patric.bjurman@se.experis.com",
                            FirstName = "Patric",
                            LastName = "Bjurman",
                            Sub = "auth0|6136084e2c6a4500693fefa9"
                        });
                });

            modelBuilder.Entity("BoxinatorAPI.Models.Domains.Shipment", b =>
                {
                    b.HasOne("BoxinatorAPI.Models.Domains.Country", "DestinationCountry")
                        .WithMany("Shipments")
                        .HasForeignKey("DestinationCountryId");

                    b.HasOne("BoxinatorAPI.Models.Domains.User", "User")
                        .WithMany("Shipments")
                        .HasForeignKey("UserId");

                    b.Navigation("DestinationCountry");

                    b.Navigation("User");
                });

            modelBuilder.Entity("BoxinatorAPI.Models.Domains.ShipmentStatus", b =>
                {
                    b.HasOne("BoxinatorAPI.Models.Domains.Shipment", "Shipment")
                        .WithMany("ShipmentStatus")
                        .HasForeignKey("ShipmentId");

                    b.Navigation("Shipment");
                });

            modelBuilder.Entity("BoxinatorAPI.Models.Domains.User", b =>
                {
                    b.HasOne("BoxinatorAPI.Models.Domains.Country", "Country")
                        .WithMany("Users")
                        .HasForeignKey("CountryId");

                    b.Navigation("Country");
                });

            modelBuilder.Entity("BoxinatorAPI.Models.Domains.Country", b =>
                {
                    b.Navigation("Shipments");

                    b.Navigation("Users");
                });

            modelBuilder.Entity("BoxinatorAPI.Models.Domains.Shipment", b =>
                {
                    b.Navigation("ShipmentStatus");
                });

            modelBuilder.Entity("BoxinatorAPI.Models.Domains.User", b =>
                {
                    b.Navigation("Shipments");
                });
#pragma warning restore 612, 618
        }
    }
}
