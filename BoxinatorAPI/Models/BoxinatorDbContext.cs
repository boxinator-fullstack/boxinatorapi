﻿using BoxinatorAPI.Models.Domains;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace BoxinatorAPI.Models
{
    public class BoxinatorDbContext : DbContext
    {
        // Tables
        public DbSet<User> Users { get; set; }
        public DbSet<Shipment> Shipments { get; set; }
        public DbSet<ShipmentStatus> ShipmentStatuses { get; set; }
        public DbSet<Country> Countries { get; set; }


        public BoxinatorDbContext([NotNullAttribute] DbContextOptions<BoxinatorDbContext> options)
           : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Seed data
            modelBuilder.Entity<User>().HasData(new User
            {
                Id = 1,
                FirstName = "Patric",
                LastName = "Bju",
                Email = "patric.bjurman@se.experis.com",
                Sub = "auth0|6136084e2c6a4500693fefa9"
            });
            modelBuilder.Entity<User>().HasData(new User
            {
                Id = 2,
                FirstName = "Emil",
                LastName = "Cle",
                Email = "emilcle@gmail.com",
                Sub = "auth0|612f2c14eeeb2e0068b262b0"
            });
            modelBuilder.Entity<User>().HasData(new User
            {
                Id = 3,
                FirstName = "Emil",
                LastName = "Clemedson",
                Email = "emil.clemedson@gmail.com",
                Sub = "auth0|6135fffc1ee4b60069411721"
            });
            modelBuilder.Entity<User>().HasData(new User
            {
                Id = 4,
                FirstName = "Christian",
                LastName = "Andersz",
                Email = "christian.andersz@se.experis.com",
                Sub = "auth0|6130b87feeeb2e0068b313ad"
            });

            modelBuilder.Entity<Country>().HasData(new Country { Id = 1, Name = "Sweden", CountryMultiplier = 0 });
            modelBuilder.Entity<Country>().HasData(new Country { Id = 2, Name = "Norway", CountryMultiplier = 0 });
            modelBuilder.Entity<Country>().HasData(new Country { Id = 3, Name = "Denmark", CountryMultiplier = 0 });
            modelBuilder.Entity<Country>().HasData(new Country { Id = 4, Name = "Argentina", CountryMultiplier = 5 });
            modelBuilder.Entity<Country>().HasData(new Country { Id = 5, Name = "Germany", CountryMultiplier = 3 });

            modelBuilder.Entity<Shipment>().HasData(new Shipment { Id = 1, DestinationCountryId = 1, UserId = 1 });
            modelBuilder.Entity<Shipment>().HasData(new Shipment { Id = 2, DestinationCountryId = 2, UserId = 2 });
            modelBuilder.Entity<Shipment>().HasData(new Shipment { Id = 3, DestinationCountryId = 3, UserId = 3 });
            modelBuilder.Entity<Shipment>().HasData(new Shipment { Id = 4, DestinationCountryId = 4, UserId = 4 });
            modelBuilder.Entity<Shipment>().HasData(new Shipment { Id = 5, DestinationCountryId = 5, UserId = 4 });

            modelBuilder.Entity<ShipmentStatus>().HasData(new ShipmentStatus { Id = 1, ShipmentId = 1, Status = ShipmentStatusType.COMPLETED, CreatedAt = new DateTime(2021, 02, 06) });
            modelBuilder.Entity<ShipmentStatus>().HasData(new ShipmentStatus { Id = 2, ShipmentId = 2, Status = ShipmentStatusType.CANCELLED, CreatedAt = new DateTime(2021, 02, 06) });
            modelBuilder.Entity<ShipmentStatus>().HasData(new ShipmentStatus { Id = 3, ShipmentId = 3, Status = ShipmentStatusType.CREATED, CreatedAt = new DateTime(2021, 02, 06) });
            modelBuilder.Entity<ShipmentStatus>().HasData(new ShipmentStatus { Id = 4, ShipmentId = 4, Status = ShipmentStatusType.INTRANSIT, CreatedAt = new DateTime(2021, 02, 06) });

            modelBuilder.Entity<ShipmentStatus>().HasData(new ShipmentStatus { Id = 5, ShipmentId = 5, Status = ShipmentStatusType.CREATED, CreatedAt = new DateTime(2021, 02, 06) });
            modelBuilder.Entity<ShipmentStatus>().HasData(new ShipmentStatus { Id = 6, ShipmentId = 5, Status = ShipmentStatusType.INTRANSIT, CreatedAt = new DateTime(2021, 03, 06) });
            modelBuilder.Entity<ShipmentStatus>().HasData(new ShipmentStatus { Id = 7, ShipmentId = 5, Status = ShipmentStatusType.RECIEVED, CreatedAt = new DateTime(2021, 04, 06) });
            modelBuilder.Entity<ShipmentStatus>().HasData(new ShipmentStatus { Id = 8, ShipmentId = 5, Status = ShipmentStatusType.COMPLETED, CreatedAt = new DateTime(2021, 05, 06) });
            modelBuilder.Entity<ShipmentStatus>().HasData(new ShipmentStatus { Id = 9, ShipmentId = 5, Status = ShipmentStatusType.CANCELLED, CreatedAt = new DateTime(2021, 06, 06) });
        }
    }
}
