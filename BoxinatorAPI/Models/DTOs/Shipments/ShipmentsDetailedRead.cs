﻿using BoxinatorAPI.Models.DTOs.Settings;
using BoxinatorAPI.Models.DTOs.ShipmentStatus;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BoxinatorAPI.Models.DTOs.Shipments
{
    public class ShipmentsDetailedRead
    {
        [Required]
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string ReceiverName { get; set; }

        [Required]
        [MaxLength(20)]
        public string BoxColour { get; set; }

        [Required]
        public int WeightOption { get; set; }

        [Required]
        public int TotalCost { get; set; }

        [Required]
        public CountryRead DestinationCountry { get; set; }

        [Required]
        public User.UserRead User { get; set; }

        public ICollection<ShipmentStatusRead> ShipmentStatus { get; set; }

        [Required]
        public int? UserId { get; set; }
    }
}
