﻿using BoxinatorAPI.Models.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BoxinatorAPI.Models.DTOs.ShipmentStatus
{
    public class ShipmentStatusRead
    {
        public string Status { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}
