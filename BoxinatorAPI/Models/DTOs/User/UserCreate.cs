﻿using BoxinatorAPI.Models.Domains;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BoxinatorAPI.Models.DTOs.User
{
    public class UserCreate
    {
        [MaxLength(100)]
        public string FirstName { get; set; }

        [MaxLength(100)]
        public string LastName { get; set; }

        [MaxLength(42)]
        public string Sub { get; set; }

        [Required]
        [MaxLength(100)]
        public string Email { get; set; }

        public DateTime? BirthDate { get; set; }

        public int? ZipCode { get; set; }

        [MaxLength(14)]
        public string Contactnumber { get; set; }

        public int? CountryId { get; set; }
    }
}
