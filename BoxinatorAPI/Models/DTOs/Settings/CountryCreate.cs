﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace BoxinatorAPI.Models.DTOs.Settings
{
    public class CountryCreate
    {
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required]
        public int CountryMultiplier { get; set; }
    }
}
