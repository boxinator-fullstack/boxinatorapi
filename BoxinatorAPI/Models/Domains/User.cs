﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BoxinatorAPI.Models.Domains
{
    public class User
    {
        // Pk
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        // Fields
        [MaxLength(100)]
        public string FirstName { get; set; }


        [MaxLength(100)]
        public string LastName { get; set; }

        [Required]
        [MaxLength(100)]
        public string Email { get; set; }

        public DateTime? BirthDate { get; set; }

        public int? ZipCode { get; set; }

        public AccountType? AccountType { get; set; }

        [MaxLength(14)]
        public string Contactnumber { get; set; }

        public int? CountryId { get; set; }

        [MaxLength(42)]
        public string Sub { get; set; }


        // Relationships
        public Country Country { get; set; }
        public ICollection<Shipment> Shipments { get; set; }
    }
}
