﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BoxinatorAPI.Models.Domains
{
    public class Country
    {
        // Pk
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        // Fields
        [MaxLength(100)]
        [Required]
        public string Name { get; set; }

        [Required]
        public int CountryMultiplier { get; set; }

        // Relationships
        public ICollection<User> Users { get; set; }
        public ICollection<Shipment> Shipments { get; set; }

    }
}
