﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BoxinatorAPI.Models.Domains
{

    public enum ShipmentStatusType
    {
        CREATED,
        RECIEVED,
        INTRANSIT,
        COMPLETED,
        CANCELLED
    }

}
