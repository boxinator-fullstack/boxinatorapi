﻿using AutoMapper;
using BoxinatorAPI.Models.Domains;
using BoxinatorAPI.Models.DTOs.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BoxinatorAPI.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserCreate>()
                .ReverseMap();
            CreateMap<User, UserEdit>()
                .ReverseMap();
            CreateMap<User, UserRead>()
                .ReverseMap();
        }
    }
}
