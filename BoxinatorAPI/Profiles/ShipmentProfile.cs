﻿using AutoMapper;
using BoxinatorAPI.Models.Domains;
using BoxinatorAPI.Models.DTOs.Shipments;
using BoxinatorAPI.Models.DTOs.ShipmentStatus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BoxinatorAPI.Profiles
{
    public class ShipmentProfile : Profile
    {
        public ShipmentProfile()
        {
            CreateMap<Shipment, ShipmentsCreate>()
                .ReverseMap();
            CreateMap<Shipment, ShipmentsEdit>()
                .ReverseMap();
            CreateMap<Shipment, ShipmentsRead>()
                .ReverseMap();
            CreateMap<Shipment, ShipmentsDetailedRead>()
                .ReverseMap();
            CreateMap<ShipmentStatusRead, ShipmentStatus>()
                .ForMember(d => d.Status, o => o.MapFrom(y => y.Status.ToLower()))
                .ReverseMap();
        }
    }
}
