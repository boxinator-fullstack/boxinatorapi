﻿using BoxinatorAPI.Models.Domains;
using FluentEmail.Core;
using FluentEmail.Razor;
using FluentEmail.Smtp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace BoxinatorAPI.Services.EmailService
{
    public class EmailSender : IEmailSender
    {
        private string _email;
        private string _password;
        private string _registerURL;

        public EmailSender(string email, string password, string registerURL)
        {
            _email = email;
            _password = password;
            _registerURL = registerURL;


            SmtpClient smtp = new SmtpClient
            {
                UseDefaultCredentials = false,
                Port = 587,
                Host = "smtp.gmail.com",
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Credentials = new NetworkCredential(_email, _password),
                EnableSsl = true,
            };
            Email.DefaultSender = new SmtpSender(smtp);
        }

        public async Task SendReceiptEmail(string to, Shipment order)
        {

            StringBuilder stringBuilder = new();
            stringBuilder.AppendLine("<h1>Thank you for your order!</h1>");
            stringBuilder.AppendLine("<h4>Order info:</h4>");
            stringBuilder.AppendLine($"<p>Receiver: {order.ReceiverName}.</p>");
            stringBuilder.AppendLine($"<p>Weight: {order.WeightOption} kg.</p>");
            stringBuilder.AppendLine($"<p>Price: {order.TotalCost} kr.</p>");
            stringBuilder.AppendLine($"<a href='{_registerURL}'>create account</a>");

            var email = Email
                .From(_email, "Boxinator")
                .To(to)
                .Subject("Thank you for your order!")
                .UsingTemplate(stringBuilder.ToString(), new {});

            var response = await email.SendAsync();
            Console.WriteLine(response.ErrorMessages);
        }
    }
}
